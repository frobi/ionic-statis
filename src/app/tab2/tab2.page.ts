import { Component } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public items: any;
  // Tambah "public http: HttpClient" dibagian constructor
  constructor(public navCtrl: NavController, public http: HttpClient) {
    //memanggil method getData
    this.getData();
  }
  // buat method getData()
  getData() {
    let url = "http://localhost:8000/api/familywedding";
    let data: Observable<any> = this.http.get(url);
    data.subscribe(result => {
      this.items = result;
      console.log(result);
    });
  }
}
